//
// Created by wignorbo on 19.11.23.
//

#include <malloc.h>
#include "queue.h"

int pair_cmp(Pair left, Pair right) {
    if (left.time > right.time) return 1;
    if (left.time < right.time) return -1;

    if (left.id > right.id) return 1;
    if (left.id < right.id) return -1;

    return 0;
}

Queue *queue_init() {
    Queue *q = (Queue*) malloc(sizeof(Queue));
    q->size = 0;
    q->replies = 0;
    return q;
}

Pair queue_pop(Queue *q) {
    Pair popped = q->pairs[0];
    for (int i = 0; i < q->size - 1; i++) {
        q->pairs[i] = q->pairs[i + 1];
    }
    q->size--;
    return popped;
}

void queue_push(Queue *q, Pair pair) {
    int index = 0;
    while (pair_cmp(pair, q->pairs[index]) > 0 && index < q->size) {
        index++;
    }
    for (int i = q->size; i > index; i--) {
        q->pairs[i] = q->pairs[i - 1];
    }
    q->pairs[index] = pair;
    q->size++;
}

void queue_print(Queue *q) {
    for (int i = 0; i < q->size; i++) {
        fprintf(stdout, "(%d, %d)", q->pairs[i].time, q->pairs[i].id);
    }
    fprintf(stdout, "\n");
    fflush(stdout);
}
