#include <stdio.h>

#include "pa2345.h"
#include "message.h"
#include "mesh.h"
#include "lamport.h"
#include "queue.h"

void wait_for_replies(const void * self);
void wait_for_process_turn(const void * self);
void handle_cs(const void * self, int sender_id, Message* msg);

int request_cs(const void * self) {
    Mesh *mesh = (Mesh*) self;
    // fprintf(stdout, "%d requests CS\n", mesh->current_id);
    // fflush(stdout);

    timestamp_t t = get_lamport_time_inc();

    Pair p = {.id = mesh->current_id, .time = t};
    queue_push(mesh->queue, p);
    // queue_print(mesh->queue);

    Message *message = message_create(CS_REQUEST, NULL, t);
    send_multicast(mesh, message);

    wait_for_replies(self);
    wait_for_process_turn(self);

    // fprintf(stdout, "%d enters CS\n", mesh->current_id);
    // fflush(stdout);
    return 0;
}

void wait_for_replies(const void * self) {
    Mesh *mesh = (Mesh*) self;
    mesh->queue->replies = 0;
    // fprintf(stdout, "%d waiting for replies...\n", mesh->current_id);
    // fflush(stdout);

    while (mesh->queue->replies != mesh->process_count - 2) {
        Message rcv;
        int sender_id = receive_any(mesh, &rcv);
        lamport_receive(rcv.s_header.s_local_time);
        handle_cs(mesh, sender_id, &rcv);
        if (rcv.s_header.s_type == CS_REPLY) {
            mesh->queue->replies++;
        }
    }
}

void wait_for_process_turn(const void * self) {
    Mesh *mesh = (Mesh*) self;
    // fprintf(stdout, "%d waiting for turn...\n", mesh->current_id);
    // fflush(stdout);

    while (mesh->queue->pairs[0].id != mesh->current_id) {
        Message rcv;
        int sender_id = receive_any(mesh, &rcv);
        lamport_receive(rcv.s_header.s_local_time);
        handle_cs(mesh, sender_id, &rcv);
    }
}

void handle_cs(const void * self, int sender_id, Message *msg) {
    Mesh *mesh = (Mesh*) self;

    if (msg->s_header.s_type == CS_REQUEST) {
        // fprintf(stdout, "%d receives CS_REQUEST from %d\n", mesh->current_id, sender_id);
        // fflush(stdout);

        queue_push(mesh->queue, (Pair) {.id = sender_id, .time = msg->s_header.s_local_time});
        // queue_print(mesh->queue);

        Message *message = message_create(CS_REPLY, NULL, get_lamport_time_inc());
        send(mesh, sender_id, message);
    } else if (msg->s_header.s_type == CS_RELEASE) {
        // fprintf(stdout, "%d receives CS_RELEASE from %d\n", mesh->current_id, sender_id);
        // fflush(stdout);

        queue_pop(mesh->queue);
        // queue_print(mesh->queue);

    } else if (msg->s_header.s_type == DONE) {
        // fprintf(stdout, "%d receives DONE from %d\n", mesh->current_id, sender_id);
        // fflush(stdout);
        mesh->done_count++;
    }
}

int release_cs(const void * self) {
    Mesh *mesh = (Mesh*) self;
    // fprintf(stdout, "%d releases CS\n", mesh->current_id);
    // fflush(stdout);

    Message *message = message_create(CS_RELEASE, NULL, get_lamport_time_inc());
    send_multicast(mesh, message);

    queue_pop(mesh->queue);
    return 0;
}

//void print(const char * s) {
//     fprintf(stdout, "%s", s);
//     fflush(stdout);
//}
