//
// Created by wignorbo on 19.11.23.
//

#ifndef PA4_QUEUE_H
#define PA4_QUEUE_H

#include "ipc.h"

typedef struct {
    local_id id;
    timestamp_t time;
} Pair;

int pair_cmp(Pair left, Pair right);

typedef struct {
    size_t size;
    Pair pairs[11];
    size_t replies;
} Queue;

Queue *queue_init();
Pair queue_pop(Queue *q);
void queue_push(Queue *q, Pair pair);
void queue_print(Queue *q);

#endif //PA4_QUEUE_H
