#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <wait.h>

#include "logger.h"
#include "mesh.h"
#include "ipc.h"
#include "pa1.h"

int usage() {
    printf("Usage: ./program -p [process count]\n");
    return 1;
}

// Вспомогательная функция для создания сообщения
Message *message_create(MessageType type, const char *body) {
    Message *msg = malloc(sizeof(Message));
    msg->s_header.s_magic = MESSAGE_MAGIC;
    msg->s_header.s_payload_len = strlen(body);
    msg->s_header.s_type = type;
    memcpy(&(msg->s_payload), body, strlen(body) + 1);
    return msg;
}

int main(int argc, const char *argv[]) {
    if (argc < 3 || strcmp(argv[1], "-p") != 0) return usage();
    int process_count = atoi(argv[2]);

    logger_init();
    Mesh *mesh = mesh_init(process_count);
    if (!mesh) {
        perror("couldn't allocate mesh");
        exit(1);
    }
    log_flush_pipe();

    for (int i = 1; i < mesh->process_count; i++) {
        switch (fork()) {
            case -1:
                exit(1);
            case 0:
                mesh->current_id = i;

                close_unused_pipes(mesh);

                // Пишем в лог, что процесс начал работу
                char buf[64];
                sprintf(buf, log_started_fmt, mesh->current_id, getpid(), getppid());
                log_event(buf);

                // Отправляем всем процессам сообщение STARTED
                Message *message = message_create(STARTED, buf);
                send_multicast(mesh, message);
                free(message);

                // Дожидаемся, пока все процессы не пришлют STARTED
                int started_count = 0;
                while (started_count != mesh->process_count - 2) {
                    for (int j = 1; j < mesh->process_count; j++) {
                        if (mesh->current_id == j) continue;

                        Message rcv;
                        if (receive(mesh, j, &rcv) == 0 && rcv.s_header.s_type == STARTED) {
                            started_count++;
//                            printf("[%d] GOT MESSAGE (%d): %s", mesh->current_id, mesh->channels[mesh->current_id][j]->fd_read, rcv.s_payload);
                        }
                    }
                }

                // Пишем в лог, что процесс получил от остальных STARTED
                sprintf(buf, log_received_all_started_fmt, mesh->current_id);
                log_event(buf);
//                printf("%s", buf);

                // Тут типа полезная работа
                for (int x = 0; x < 100000; x++);

                // Пишем в лог, что процесс завершил работу
                sprintf(buf, log_done_fmt, mesh->current_id);
                log_event(buf);

                // Отправляем всем процессам сообщение DONE
                message = message_create(DONE, buf);
                send_multicast(mesh, message);
                free(message);

                // Дожидаемся, пока все процессы не пришлют DONE
                int done_count = 0;
                while (done_count != mesh->process_count - 2) {
                    for (int j = 1; j < mesh->process_count; j++) {
                        if (mesh->current_id == j) continue;

                        Message rcv;
                        if (receive(mesh, j, &rcv) == 0 && rcv.s_header.s_type == DONE) {
                            done_count++;
//                            printf("[%d] GOT MESSAGE: %s", mesh->current_id, rcv.s_payload);
                        }
                    }
                }

                // Пишем в лог, что процесс получил от остальных DONE
                sprintf(buf, log_received_all_done_fmt, mesh->current_id);
                log_event(buf);
//                printf("%s", buf);

                // Завершаем работу процесса
                exit(0);
        }
    }

    mesh->current_id = 0;
    close_unused_pipes(mesh);

    // Дожидаемся сообщений от дочерних процессов о начале работы

    for (int i = 1; i < mesh->process_count - 1; i++) {
        Message rcv;
        rcv.s_header.s_type = -1;
        while (rcv.s_header.s_type != STARTED) {
            receive(mesh, i, &rcv);
        }
//        printf("[PARENT] GOT MESSAGE: %s", rcv.s_payload);
    }

    // Дожидаемся сообщений от дочерних процессов о заверешении работы

    for (int i = 1; i < mesh->process_count - 1; i++) {
        Message rcv;
        rcv.s_header.s_type = -1;
        while (rcv.s_header.s_type != DONE) {
            receive(mesh, i, &rcv);
        }
//        printf("[PARENT] GOT MESSAGE: %s", rcv.s_payload);
    }

    // Дожидаемся, пока дочерние процессы закончат работу

    for (int i = 1; i < mesh->process_count; i++) {
        if (wait(NULL) == -1) {
            perror("error on waiting for the child process to finish");
            exit(1);
        }
    }

    mesh_free(mesh);
    logger_finalize();

    return 0;
}
