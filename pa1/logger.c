//
// Created by wignorbo on 19.09.23.
//

#include <stdio.h>
#include "logger.h"
#include "common.h"

FILE *event_file;
FILE *pipe_file;

void logger_init() {
    // Truncate files
    fclose(fopen(events_log, "w"));
    fclose(fopen(pipes_log, "w"));

    // Open files in appending mode
    event_file = fopen(events_log, "a");
    pipe_file = fopen(pipes_log, "a");
}

void log_event(char *msg) {
    fprintf(event_file, "%s", msg);
#ifdef DEBUG_PRINT
    printf("%s", msg);
#endif
}

void log_pipe(char *msg) {
    fprintf(pipe_file, "%s", msg);
#ifdef DEBUG_PRINT
    printf("%s", msg);
#endif
}

void log_flush_pipe() {
    fflush(pipe_file);
}

void logger_finalize() {
    fclose(event_file);
    fclose(pipe_file);
}
