#include <stdio.h>
#include <unistd.h>

#include "ipc.h"
#include "mesh.h"

/** Send a message to the process specified by id.
 *
 * @param self    Any data structure implemented by students to perform I/O
 * @param dst     ID of recepient
 * @param msg     Message to send
 *
 * @return 0 on success, any non-zero value on error
 */
int send(void *self, local_id dst, const Message *msg) {
    Mesh *mesh = (Mesh *) self;

    ssize_t w_bytes = write(mesh->channels[mesh->current_id][dst]->fd_write, msg,
                            sizeof(MessageHeader) + msg->s_header.s_payload_len);
    if (w_bytes != sizeof(MessageHeader) + msg->s_header.s_payload_len) {
        perror("Couldn't send Message");
        return -1;
    }

    return 0;
}

//------------------------------------------------------------------------------

/** Send multicast message.
 *
 * Send msg to all other processes including parrent.
 * Should stop on the first error.
 *
 * @param self    Any data structure implemented by students to perform I/O
 * @param msg     Message to multicast.
 *
 * @return 0 on success, any non-zero value on error
 */
int send_multicast(void *self, const Message *msg) {
    Mesh *mesh = (Mesh *) self;
    for (int i = 0; i < mesh->process_count; i++) {
        if (i == mesh->current_id) continue;
        send(mesh, i, msg);
    }
    return 0;
}

//------------------------------------------------------------------------------

/** Receive a message from the process specified by id.
 *
 * Might block depending on IPC settings.
 *
 * @param self    Any data structure implemented by students to perform I/O
 * @param from    ID of the process to receive message from
 * @param msg     Message structure allocated by the caller
 *
 * @return 0 on success, any non-zero value on error
 */
int receive(void *self, local_id from, Message *msg) {
    Mesh *mesh = (Mesh *) self;

    ssize_t r_bytes = read(mesh->channels[mesh->current_id][from]->fd_read, &(msg->s_header), sizeof(MessageHeader));
    if (r_bytes != sizeof(MessageHeader)) {
        perror("Couldn't read MessageHeader");
        return -1;
    }

    r_bytes = read(mesh->channels[mesh->current_id][from]->fd_read, msg->s_payload, msg->s_header.s_payload_len);
    if (r_bytes != msg->s_header.s_payload_len) {
        perror("Couldn't read payload");
        return -1;
    }

    return 0;
}

//------------------------------------------------------------------------------

/** Receive a message from any process.
 *
 * Receive a message from any process, in case of blocking I/O should be used
 * with extra care to avoid deadlocks.
 *
 * @param self    Any data structure implemented by students to perform I/O
 * @param msg     Message structure allocated by the caller
 *
 * @return 0 on success, any non-zero value on error
 */
// cppcheck-suppress unusedFunction
int receive_any(void *self, Message *msg) {
    Mesh *mesh = (Mesh *) self;
    while (1) {
        for (int i = 0; i < mesh->process_count; i++) {
            if (i == mesh->current_id) continue;
            if (receive(self, i, msg) == 0) {
                return 0;
            }
        }
    }
}
