//
// Created by wignorbo on 20.10.23.
//

#include "banking.h"
#include "lamport.h"


static timestamp_t lamport_time = 0;

timestamp_t get_lamport_time() {
    return lamport_time;
}

timestamp_t get_lamport_time_inc() {
    return ++lamport_time;
}

timestamp_t lamport_receive(timestamp_t received_time) {
    lamport_time = (lamport_time > received_time) ? lamport_time : received_time;
    return get_lamport_time_inc();
}
