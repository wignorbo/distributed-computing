//
// Created by wignorbo on 19.09.23.
//

#ifndef DISTRIBUTED_COMPUTING_LOGGER_H
#define DISTRIBUTED_COMPUTING_LOGGER_H

//#define DEBUG_PRINT

void logger_init();

void log_event(char *msg);

void log_pipe(char *msg);

void log_flush_pipe();

void logger_finalize();

#endif //DISTRIBUTED_COMPUTING_LOGGER_H
