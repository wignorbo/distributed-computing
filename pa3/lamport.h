//
// Created by wignorbo on 20.10.23.
//

#ifndef DISTRIBUTED_COMPUTING_LAMPORT_H
#define DISTRIBUTED_COMPUTING_LAMPORT_H

#include "ipc.h"

timestamp_t get_lamport_time_inc();

timestamp_t lamport_receive(timestamp_t received_time);

#endif //DISTRIBUTED_COMPUTING_LAMPORT_H
