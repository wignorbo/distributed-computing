#include <stdio.h>
#include "pa2345.h"
#include "message.h"
#include "mesh.h"
#include "lamport.h"

void wait_for_replies(const void *self);

void wait_for_process_turn(const void *self);

void handle_cs(const void *self, int sender_id, Message *msg);

int request_cs(const void *self) {
    Mesh *mesh = (Mesh *) self;
    // fprintf(stdout, "%d requests CS\n", mesh->current_id);
    // fflush(stdout);

    timestamp_t t = get_lamport_time_inc();

    Message *message = message_create(CS_REQUEST, NULL, t);
    send_multicast(mesh, message);

    mesh->cs_request_time = t;
    wait_for_replies(self);

    // fprintf(stdout, "%d enters CS\n", mesh->current_id);
    // fflush(stdout);
    return 0;
}

void wait_for_replies(const void *self) {
    Mesh *mesh = (Mesh *) self;
    mesh->replies = 0;

    // fprintf(stdout, "%d waiting for replies...\n", mesh->current_id);
    // fflush(stdout);

    while (mesh->replies != mesh->process_count - 2) {
        Message rcv;
        int sender_id = receive_any(mesh, &rcv);
        lamport_receive(rcv.s_header.s_local_time);
        handle_cs(mesh, sender_id, &rcv);
    }
}

void handle_cs(const void *self, int sender_id, Message *msg) {
    Mesh *mesh = (Mesh *) self;

    if (msg->s_header.s_type == CS_REQUEST) {
        // fprintf(stdout, "%d receives CS_REQUEST from %d\n", mesh->current_id, sender_id);
        // fflush(stdout);

        timestamp_t sender_t = msg->s_header.s_local_time;
        if (mesh->cs_request_time > sender_t || (mesh->cs_request_time == sender_t && mesh->current_id > sender_id)) {
            // fprintf(stdout, "%d send CS_REPLY to %d\n", mesh->current_id, sender_id);
            // fflush(stdout);
            Message *message = message_create(CS_REPLY, NULL, get_lamport_time_inc());
            send(mesh, sender_id, message);
        } else {
            // fprintf(stdout, "%d deferred CS_REQUEST from %d\n", mesh->current_id, sender_id);
            // fflush(stdout);
            mesh->deferred[sender_id] = 1;
        }
    } else if (msg->s_header.s_type == CS_REPLY) {
        // fprintf(stdout, "%d receives CS_REPLY from %d\n", mesh->current_id, sender_id);
        // fflush(stdout);
        mesh->replies++;
    } else if (msg->s_header.s_type == DONE) {
        mesh->done_count++;
    }
}

int release_cs(const void *self) {
    Mesh *mesh = (Mesh *) self;
    // fprintf(stdout, "%d releases CS\n", mesh->current_id);
    // fflush(stdout);

    Message *message = message_create(CS_REPLY, NULL, get_lamport_time_inc());
    for (int i = 1; i < mesh->process_count; i++) {
        if (i == mesh->current_id) continue;

        if (mesh->deferred[i]) {
            send(mesh, i, message);
            mesh->deferred[i] = 0;
        }
    }

    return 0;
}

//void print(const char * s) {
//     // fprintf(stdout, "%s", s);
//     // fflush(stdout);
//}
