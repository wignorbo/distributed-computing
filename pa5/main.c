#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <wait.h>

#include "logger.h"
#include "mesh.h"
#include "ipc.h"
#include "pa2345.h"
#include "lamport.h"
#include "message.h"

int usage() {
    printf("Usage: ./program -p {process count} [--mutexl]\n");
    return 1;
}

int main(int argc, const char *argv[]) {
    if (argc < 3) return usage();

    int mutexl = 0;
    int process_count = 0;
    if (strcmp(argv[1], "--mutexl") == 0) {
        mutexl = 1;
        process_count = atoi(argv[3]);
    } else if (argc == 4 && strcmp(argv[3], "--mutexl") == 0) {
        mutexl = 1;
        process_count = atoi(argv[2]);
    } else {
        mutexl = 0;
        process_count = atoi(argv[2]);
    }

    logger_init();
    Mesh *mesh = mesh_init(process_count);
    if (!mesh) {
        perror("couldn't allocate mesh");
        exit(1);
    }
    log_flush_pipe();

    for (int i = 1; i < mesh->process_count; i++) {
        switch (fork()) {
            case -1:
                exit(1);
            case 0:
                mesh->current_id = i;
                close_unused_pipes(mesh);

                // Пишем в лог, что процесс начал работу
                char buf[4096];
                sprintf(buf, log_started_fmt, 0, mesh->current_id, getpid(), getppid(), 0);
                log_event(buf);

                // Отправляем всем процессам сообщение STARTED
                Message *message = message_create(STARTED, buf, get_lamport_time_inc());
                send_multicast(mesh, message);
                free(message);

                // Дожидаемся, пока все процессы не пришлют STARTED
                int started_count = 0;
                while (started_count != mesh->process_count - 2) {
                    for (int j = 1; j < mesh->process_count; j++) {
                        if (mesh->current_id == j) continue;

                        Message rcv;
                        if (receive(mesh, j, &rcv) == 0 && rcv.s_header.s_type == STARTED) {
                            started_count++;
                            // fprintf(stdout, "[%d] GOT MESSAGE (%d): %s", mesh->current_id, mesh->channels[mesh->current_id][j]->fd_read, rcv.s_payload);
                            // fflush(stdout);
                        }
                        lamport_receive(rcv.s_header.s_local_time);
                    }
                }

                // Пишем в лог, что процесс получил от остальных STARTED
                sprintf(buf, log_received_all_started_fmt, 0, mesh->current_id);
                log_event(buf);

                // Тут типа полезная работа
                int iterations = 5 * mesh->current_id;
                for (int i = 1; i <= iterations; i++) {
                    if (mutexl) request_cs(mesh);
                    sprintf(buf, log_loop_operation_fmt, mesh->current_id, i, iterations);
                    print(buf);
                    if (mutexl) release_cs(mesh);
                }

                // Пишем в лог, что процесс завершил работу
                sprintf(buf, log_done_fmt, 0, mesh->current_id, 0);
                log_event(buf);

                // Отправляем всем процессам сообщение DONE
                message = message_create(DONE, buf, get_lamport_time_inc());
                send_multicast(mesh, message);

                while (mesh->done_count != mesh->process_count - 2) {
                    Message rcv;
                    int sender_id = receive_any(mesh, &rcv);
                    if (rcv.s_header.s_type == DONE) {
                        mesh->done_count++;
                    } else if (rcv.s_header.s_type == CS_REQUEST) {
                        message = message_create(CS_REPLY, NULL, get_lamport_time());
                        send(mesh, sender_id, message);
                    }
                }

                sprintf(buf, log_received_all_done_fmt, 0, mesh->current_id);
                log_event(buf);

                // Завершаем работу процесса
                exit(0);
        }
    }

    mesh->current_id = 0;
    close_unused_pipes(mesh);

    // Дожидаемся сообщений от дочерних процессов о начале работы

    for (int i = 1; i < mesh->process_count - 1; i++) {
        Message rcv;
        rcv.s_header.s_type = -1;
        while (rcv.s_header.s_type != STARTED) {
            receive(mesh, i, &rcv);
        }
        // fprintf(stdout, "[PARENT] GOT MESSAGE: %s", rcv.s_payload);
        // fflush(stdout);
    }

    // Дожидаемся сообщений от дочерних процессов о заверешении работы

    for (int i = 1; i < mesh->process_count - 1; i++) {
        Message rcv;
        rcv.s_header.s_type = -1;
        while (rcv.s_header.s_type != DONE) {
            receive(mesh, i, &rcv);
        }
        // fprintf(stdout, "[PARENT] GOT MESSAGE: %s", rcv.s_payload);
        // fflush(stdout);
    }

    // Дожидаемся, пока дочерние процессы закончат работу

    for (int i = 1; i < mesh->process_count; i++) {
        if (wait(NULL) == -1) {
            perror("error on waiting for the child process to finish");
            exit(1);
        }
    }

    mesh_free(mesh);
    logger_finalize();

    return 0;
}
