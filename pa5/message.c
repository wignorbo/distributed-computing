//
// Created by wignorbo on 19.11.23.
//

#include <malloc.h>
#include <string.h>
#include "message.h"


Message *message_create(MessageType type, const char *body, timestamp_t time) {
    Message *msg = malloc(sizeof(Message));
    msg->s_header.s_magic = MESSAGE_MAGIC;
    msg->s_header.s_payload_len = body == NULL ? 0 : strlen(body);
    msg->s_header.s_type = type;
    msg->s_header.s_local_time = time;
    if (body != NULL)
        memcpy(&(msg->s_payload), body, strlen(body) + 1);
    return msg;
}
