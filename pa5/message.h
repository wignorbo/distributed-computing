//
// Created by wignorbo on 19.11.23.
//

#ifndef PA4_MESSAGE_H
#define PA4_MESSAGE_H

#include "ipc.h"

Message *message_create(MessageType type, const char *body, timestamp_t time);

#endif //PA4_MESSAGE_H
