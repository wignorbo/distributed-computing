//
// Created by wignorbo on 19.09.23.
//

#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mesh.h"
#include "logger.h"


void make_nonblock(int fd) {
    int flags = fcntl(fd, F_GETFL, 0);
    fcntl(fd, F_SETFL, flags | O_NONBLOCK);
}


Mesh *mesh_init(int children_count) {
    Mesh *mesh = malloc(sizeof(Mesh));
    if (!mesh) {
        return NULL;
    }
    mesh->current_id = 0;
    mesh->process_count = children_count + 1;
    mesh->replies = 0;
    mesh->done_count = 0;
    memset(mesh->deferred, 0, sizeof(mesh->deferred));

    int fd[2];
    for (int i = 0; i < mesh->process_count; i++) {
        for (int j = i + 1; j < mesh->process_count; j++) {
            Channel *ch1 = malloc(sizeof(Channel));
            Channel *ch2 = malloc(sizeof(Channel));
            if (!ch1 || !ch2) {
                perror("unable to malloc ipc channels");
                exit(1);
            }
            if (pipe(fd)) {
                perror("Unable to pipe");
                exit(1);
            }
            ch1->fd_read = fd[0];
            ch2->fd_write = fd[1];

            if (pipe(fd)) {
                perror("Unable to pipe");
                exit(1);
            }
            ch2->fd_read = fd[0];
            ch1->fd_write = fd[1];

            mesh->channels[i][j] = ch1;
            mesh->channels[j][i] = ch2;

            make_nonblock(ch1->fd_read);
            make_nonblock(ch1->fd_write);
            make_nonblock(ch2->fd_read);
            make_nonblock(ch2->fd_write);

            char msg[256];
            sprintf(msg, "OPEN CHANNEL [%d <-> %d]\n"
                         "[%d] READS FROM [%d] AND WRITES TO [%d]\n"
                         "[%d] READS FROM [%d] AND WRITES TO [%d]\n",
                    i, j,
                    i, ch1->fd_read, ch1->fd_write,
                    j, ch2->fd_read, ch2->fd_write);
            log_pipe(msg);
        }
    }

    return mesh;
}

void close_unused_pipes(Mesh *mesh) {
    for (int i = 0; i < mesh->process_count; i++) {
        for (int j = 0; j < mesh->process_count; j++) {
            if (i != mesh->current_id && i != j) {
                close(mesh->channels[i][j]->fd_read);
                close(mesh->channels[i][j]->fd_write);
            }
        }
    }
}

void mesh_free(Mesh *mesh) {
    for (int i = 0; i < mesh->process_count; i++) {
        for (int j = 0; j < mesh->process_count; j++) {
            if (i == j) continue;
            close(mesh->channels[i][j]->fd_read);
            close(mesh->channels[i][j]->fd_write);
            free(mesh->channels[i][j]);
        }
    }

    free(mesh);
}
