#include <errno.h>
#include <stdio.h>
#include <unistd.h>

#include "ipc.h"
#include "mesh.h"

/** Send a message to the process specified by id.
 *
 * @param self    Any data structure implemented by students to perform I/O
 * @param dst     ID of recepient
 * @param msg     Message to send
 *
 * @return 0 on success, any non-zero value on error
 */
int send(void *self, local_id dst, const Message *msg) {
    Mesh *mesh = (Mesh *) self;
    size_t msg_size = sizeof(MessageHeader) + msg->s_header.s_payload_len;
    ssize_t w_bytes = write(mesh->channels[mesh->current_id][dst]->fd_write, msg,
                            msg_size);
    return w_bytes == msg_size ? 0 : -1;
}

//------------------------------------------------------------------------------

/** Send multicast message.
 *
 * Send msg to all other processes including parrent.
 * Should stop on the first error.
 *
 * @param self    Any data structure implemented by students to perform I/O
 * @param msg     Message to multicast.
 *
 * @return 0 on success, any non-zero value on error
 */
int send_multicast(void *self, const Message *msg) {
    Mesh *mesh = (Mesh *) self;
    for (int i = 0; i < mesh->process_count; i++) {
        if (i == mesh->current_id) continue;
        if (send(mesh, i, msg) == -1) {
            return -1;
        }
    }
    return 0;
}

int read_header(int fd, Message *msg) {
    size_t header_size = sizeof(msg->s_header);
    ssize_t r_bytes = read(fd, &msg->s_header, header_size);
    if ((r_bytes < 0 && errno == EAGAIN) || r_bytes == 0)
        return 1;

    return r_bytes == header_size ? 0 : -1;
}

int read_payload(int fd, Message *msg) {
    if (msg->s_header.s_payload_len == 0)
        return 0;

    while (1) {
        ssize_t r_bytes = read(fd, msg->s_payload, msg->s_header.s_payload_len);
        if (r_bytes < 0 && errno == EAGAIN) continue;
        return r_bytes == msg->s_header.s_payload_len ? 0 : -1;
    }
}

//------------------------------------------------------------------------------

/** Receive a message from the process specified by id.
 *
 * Might block depending on IPC settings.
 *
 * @param self    Any data structure implemented by students to perform I/O
 * @param from    ID of the process to receive message from
 * @param msg     Message structure allocated by the caller
 *
 * @return 0 on success, any non-zero value on error
 */
int receive(void *self, local_id from, Message *msg) {
    Mesh *mesh = (Mesh *) self;
    while (1) {
        int status = read_header(mesh->channels[mesh->current_id][from]->fd_read, msg);
        if (status == 1) continue;
        if (status == 0) break;
        return -1;
    }
    return read_payload(mesh->channels[mesh->current_id][from]->fd_read, msg);
}

//------------------------------------------------------------------------------

/** Receive a message from any process.
 *
 * Receive a message from any process, in case of blocking I/O should be used
 * with extra care to avoid deadlocks.
 *
 * @param self    Any data structure implemented by students to perform I/O
 * @param msg     Message structure allocated by the caller
 *
 * @return 0 on success, any non-zero value on error
 */
// cppcheck-suppress unusedFunction
int receive_any(void *self, Message *msg) {
    Mesh *mesh = (Mesh *) self;
    while (1) {
        for (int i = 0; i < mesh->process_count; i++) {
            if (i == mesh->current_id) continue;

            int status = read_header(mesh->channels[mesh->current_id][i]->fd_read, msg);
            if (status == 1) continue;
            if (status == -1) return -1;

            status = read_payload(mesh->channels[mesh->current_id][i]->fd_read, msg);
            if (status < 0) return status;

            return i;
        }
    }
}
