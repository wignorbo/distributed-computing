//
// Created by wignorbo on 19.09.23.
//

#ifndef DISTRIBUTED_COMPUTING_MESH_H
#define DISTRIBUTED_COMPUTING_MESH_H

#include "ipc.h"

typedef struct {
    int fd_read;
    int fd_write;
} Channel;

typedef struct {
    int current_id;
    int process_count;
    int done_count;
    int replies;
    timestamp_t cs_request_time;
    int deferred[11];
    Channel *channels[11][11];
} Mesh;

Mesh *mesh_init(int children_count);

void close_unused_pipes(Mesh *mesh);

void mesh_free(Mesh *mesh);

#endif //DISTRIBUTED_COMPUTING_MESH_H
