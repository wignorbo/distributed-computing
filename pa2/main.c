#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <wait.h>

#include "logger.h"
#include "mesh.h"
#include "ipc.h"
#include "banking.h"
#include "pa2345.h"

int usage() {
    fprintf(stdout, "Usage: ./program -p [process count] [money1] [money2] ...\n");
    fflush(stdout);
    return 1;
}

// Вспомогательная функция для создания сообщения
Message *message_create(MessageType type, void *body, size_t size) {
    Message *msg = (Message *) malloc(sizeof(Message));
    msg->s_header.s_magic = MESSAGE_MAGIC;
    msg->s_header.s_payload_len = size;
    msg->s_header.s_type = type;
    memcpy(&(msg->s_payload), body, msg->s_header.s_payload_len);
    return msg;
}

void transfer(void *parent_data, local_id src, local_id dst, balance_t amount) {
    Mesh *mesh = (Mesh *) parent_data;
    TransferOrder transfer_order = {
            .s_src = src,
            .s_dst = dst,
            .s_amount = amount
    };
    Message *msg = message_create(TRANSFER, &transfer_order, sizeof(TransferOrder));
    assert(send(mesh, src, msg) == 0);

    Message ack_message;
    assert(receive(mesh, dst, &ack_message) == 0);
}

void appendToHistory(BalanceHistory *bh, BalanceState state) {
    for (int i = bh->s_history_len; i < state.s_time; i++) {
        if (i == 0) continue;
        bh->s_history[i] = bh->s_history[i - 1];
        bh->s_history[i].s_time = i;
    }
    bh->s_history[state.s_time] = state;
    bh->s_history_len = state.s_time + 1;
//    fprintf(stdout, "[proc=%d] append to history: [time=%d, balance=%d]\n", bh->s_id, state.s_time, state.s_balance);
//    fflush(stdout);
}

BalanceHistory init_balance_history(local_id id) {
    BalanceHistory history = {
            .s_id = id,
            .s_history_len = 0
    };
    memset(history.s_history, 0, sizeof(history.s_history));
    return history;
}

void print_balance_state(BalanceState balance_state, int id) {
    fprintf(stdout, "BalanceState(parent=%d, s_time=%d, s_balance=%d, s_balance_pending=%d)\n", id,
            balance_state.s_time, balance_state.s_balance, balance_state.s_balance_pending_in);
    fflush(stdout);
}

void print_balance_history(BalanceHistory balance_history) {
    fprintf(stdout, "--- BalanceHistory (s_id=%d, s_length=%d)\n", balance_history.s_id, balance_history.s_history_len);
    for (int i = 0; i < balance_history.s_history_len; i++) {
        print_balance_state(balance_history.s_history[i], balance_history.s_id);
    }
}

int main(int argc, const char *argv[]) {
    if (argc < 3 || strcmp(argv[1], "-p") != 0) return usage();
    int process_count = atoi(argv[2]);

    int money[process_count + 1];
    for (int i = 1; i < process_count + 1; i++) {
        money[i] = atoi(argv[2 + i]);
    }

    logger_init();
    Mesh *mesh = mesh_init(process_count);
    if (!mesh) {
        perror("couldn't allocate mesh");
        exit(1);
    }
    log_flush_pipe();

    BalanceHistory history;
    BalanceState balance_state;

    for (int i = 1; i < mesh->process_count; i++) {
        switch (fork()) {
            case -1:
                exit(1);
            case 0:
                mesh->current_id = i;
                close_unused_pipes(mesh);

                history = init_balance_history(i);
                balance_state.s_time = 0;
                balance_state.s_balance = money[i];
                balance_state.s_balance_pending_in = 0;
                appendToHistory(&history, balance_state);

                // Пишем в лог, что процесс начал работу
                char buf[128];
                sprintf(buf, log_started_fmt, balance_state.s_time, mesh->current_id, getpid(), getppid(),
                        balance_state.s_balance);
                log_event(buf);

                // Отправляем всем процессам сообщение STARTED
                Message *message = message_create(STARTED, buf, sizeof(buf));
                send_multicast(mesh, message);

                // Дожидаемся, пока все процессы не пришлют STARTED
                int started_count = 0;
                while (started_count != mesh->process_count - 2) {
                    for (int j = 1; j < mesh->process_count; j++) {
                        if (mesh->current_id == j) continue;

                        Message rcv;
                        if (receive(mesh, j, &rcv) == 0 && rcv.s_header.s_type == STARTED)
                            started_count++;
                    }
                }

                // Пишем в лог, что процесс получил от остальных STARTED
                sprintf(buf, log_received_all_started_fmt, balance_state.s_time, mesh->current_id);
                log_event(buf);

                // Тут типа полезная работа

                int done_count = 0;
                int stop = 0;
                timestamp_t end_time = 0;

                while (done_count != mesh->process_count - 2 || !stop) {
                    Message rcv;
                    receive_any(mesh, &rcv);
                    switch (rcv.s_header.s_type) {
                        case TRANSFER: {
                            TransferOrder order;
                            memcpy(&order, rcv.s_payload, rcv.s_header.s_payload_len);

                            if (history.s_id == order.s_dst) {
                                balance_state.s_time = get_physical_time();
                                balance_state.s_balance += order.s_amount;
                                appendToHistory(&history, balance_state);
                                sprintf(buf, log_transfer_in_fmt, balance_state.s_time, order.s_dst, order.s_amount,
                                        order.s_src);
                                log_event(buf);
                                Message *ack = message_create(ACK, NULL, 0);
                                assert(send(mesh, PARENT_ID, ack) == 0);
                            } else if (history.s_id == order.s_src) {
                                assert(send(mesh, order.s_dst, &rcv) == 0);
                                balance_state.s_time = get_physical_time();
                                balance_state.s_balance -= order.s_amount;
                                appendToHistory(&history, balance_state);
                                sprintf(buf, log_transfer_out_fmt, balance_state.s_time, order.s_src, order.s_amount,
                                        order.s_dst);
                                log_event(buf);
                            }
                        }
                            break;
                        case STOP:
                            // Отправляем всем процессам сообщение DONE
                            stop = 1;
//                            fprintf(stdout, "[%d] GOT STOP MESSAGE\n", mesh->current_id);
//                            fflush(stdout);
                            message = message_create(DONE, buf, sizeof(buf));
//                            fprintf(stdout, "[%d] BEFORE DONE MULTICAST\n", mesh->current_id);
//                            fflush(stdout);
                            send_multicast(mesh, message);
//                            fprintf(stdout, "[%d] SENT DONE TO EVERYONE\n", mesh->current_id);
//                            fflush(stdout);
                            end_time = get_physical_time();
                            break;
                        case DONE:
                            done_count++;
//                            fprintf(stdout, "[%d] GOT DONE MESSAGE: done_count=%d\n", mesh->current_id, done_count);
//                            fflush(stdout);
                            break;
                        default:
                            fprintf(stderr, "GOT HEADER %d\n", rcv.s_header.s_type);
                            perror("Process received unknown header\n");
                            fflush(stderr);
                            exit(1);
                    }
                }

//                fprintf(stdout, "[%d] EXIT LOOP, stop=%d, done_count=%d\n", mesh->current_id, stop, done_count);
//                fflush(stdout);

                balance_state = history.s_history[history.s_history_len - 1];
                balance_state.s_time = end_time;
                appendToHistory(&history, balance_state);

                // Пишем в лог, что процесс завершил работу
                sprintf(buf, log_done_fmt, balance_state.s_time, mesh->current_id, balance_state.s_balance);
                log_event(buf);

                // Пишем в лог, что процесс получил от остальных DONE
                sprintf(buf, log_received_all_done_fmt, balance_state.s_time, mesh->current_id);
                log_event(buf);

//                print_balance_history(history);
                Message *msg = message_create(BALANCE_HISTORY, &history,
                                              history.s_history_len * sizeof(BalanceState) +
                                              sizeof(history.s_history_len) + sizeof(history.s_id));


//                fprintf(stdout, "[%d] SENDING HISTORY TO PARENT\n", mesh->current_id);
//                fflush(stdout);
                assert(send(mesh, PARENT_ID, msg) == 0);
//                fprintf(stdout, "[%d] SENT HISTORY TO PARENT\n", mesh->current_id);
//                fflush(stdout);

                // Завершаем работу процесса
//                fprintf(stdout, "[%d] EXIT\n", mesh->current_id);
//                fflush(stdout);
                exit(0);
        }
    }

    mesh->current_id = 0;
    close_unused_pipes(mesh);

    // Дожидаемся сообщений от дочерних процессов о начале работы

    for (int i = 1; i < mesh->process_count; i++) {
        Message rcv;
        rcv.s_header.s_type = -1;
        while (rcv.s_header.s_type != STARTED)
            receive(mesh, i, &rcv);
    }

    bank_robbery(mesh, mesh->process_count - 1);

    // Дожидаемся сообщений от дочерних процессов о заверешении работы
//    for (int i = 1; i < mesh->process_count; i++) {
//        Message rcv;
//        rcv.s_header.s_type = -1;
//        fprintf(stdout, "PARENT GETTING ACK FROM %d\n", i);
//        fflush(stdout);
//        while (rcv.s_header.s_type != ACK) {
//            receive(mesh, i, &rcv);
//        }
//        fprintf(stdout, "PARENT GOT ACK FROM %d\n", i);
//        fflush(stdout);
//    }

//    fprintf(stdout, "PARENT SENDING STOP TO CHILDREN\n");
//    fflush(stdout);
    Message *stop = message_create(STOP, NULL, 0);
    send_multicast(mesh, stop);
//    fprintf(stdout, "PARENT SENT STOP TO CHILDREN\n");
//    fflush(stdout);

    for (int i = 1; i < mesh->process_count; i++) {
        Message rcv;
        rcv.s_header.s_type = -1;
//        fprintf(stdout, "PARENT GETTING DONE FROM %d\n", i);
//        fflush(stdout);
        while (rcv.s_header.s_type != DONE)
            receive(mesh, i, &rcv);
//        fprintf(stdout, "PARENT GOT DONE FROM %d\n", i);
//        fflush(stdout);
    }

    AllHistory all_history = {
            .s_history_len = mesh->process_count - 1
    };

    for (int i = 1; i < mesh->process_count; i++) {
        Message rcv;
        rcv.s_header.s_type = -1;
//        fprintf(stdout, "PARENT READING HISTORY FROM %d\n", i);
//        fflush(stdout);
        while (rcv.s_header.s_type != BALANCE_HISTORY) {
            receive(mesh, i, &rcv);
        }
//        fprintf(stdout, "PARENT GOT HISTORY FROM %d\n", i);
//        fflush(stdout);

        BalanceHistory child_history;
        memcpy(&child_history, rcv.s_payload, rcv.s_header.s_payload_len);
        all_history.s_history[i - 1] = child_history;
    }

//    fprintf(stdout, "PARENT GOT ALL HISTORIES\n");
//    fflush(stdout);

    print_history(&all_history);

    // Дожидаемся, пока дочерние процессы закончат работу

    for (int i = 1; i < mesh->process_count; i++) {
        if (wait(NULL) == -1) {
            perror("error on waiting for the child process to finish");
            exit(1);
        }
    }

    mesh_free(mesh);
    logger_finalize();

    return 0;
}
