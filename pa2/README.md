# Лабораторная работа №2

## Установка и запуск

```
docker build --platform linux/386 . -t pa2
docker run --platform linux/386 pa2 -p <PROCESS_COUNT> <MONEY1> <MONEY2> ...
```

Вот так можно запустить, чтобы проверить на дедлоки:

```
docker-compose up --build && while true; do docker-compose up; done
```
